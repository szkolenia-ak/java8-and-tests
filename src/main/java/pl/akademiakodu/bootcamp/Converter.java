package pl.akademiakodu.bootcamp;

@FunctionalInterface
public interface Converter<F, T> {
    T convert(F from);
}

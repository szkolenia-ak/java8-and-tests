package pl.akademiakodu.bootcamp;

public class BiComparatorImpl implements BiComparator<Integer, String, Integer> {
    @Override
    public Integer compare(Integer integer, String s) {
        Integer i = Integer.valueOf(s);
        int i1 = (integer < i) ? -1 : (integer > i ? 1 : 0);
//        int ret = i - integer;
        //return Integer.valueOf(s) - integer;
        return i1;
    }
}

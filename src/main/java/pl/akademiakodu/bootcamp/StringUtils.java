package pl.akademiakodu.bootcamp;

import java.util.Optional;

public class StringUtils {
    Optional<String> getFirsLetterUppercase(String s){
        if(s == null || s.isEmpty()){
            return Optional.empty();
            //return null;
        }
        return Optional.of(s.substring(0,1).toUpperCase());
//        return s.substring(0,1).toUpperCase();
    }

    String getFirsLetterUppercaseNoOptional(String s){
        if(s == null || s.isEmpty()){
            return null;
        }
        return s.substring(0,1).toUpperCase();
    }
}

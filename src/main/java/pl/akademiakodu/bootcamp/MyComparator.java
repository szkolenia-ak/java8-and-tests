package pl.akademiakodu.bootcamp;

@FunctionalInterface
public interface MyComparator<Typ, Ret> {

    Ret compare(Typ i1, Typ i2);
   // Ret compare(Typ x1);
}

package pl.akademiakodu.bootcamp;

@FunctionalInterface
public interface BiComparator<T1, T2, R> {

    R compare(T1 t1, T2 t2);

}

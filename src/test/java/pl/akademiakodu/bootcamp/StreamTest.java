package pl.akademiakodu.bootcamp;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StreamTest {

    List<String> stringList =
            Arrays.asList("aaa", "bbb", "cccd", "abc"
                    , "bca", "qwev", "df", "aaaa", "x");

    @Test
    public void streamFiltering(){
        Stream<String> stream = stringList.stream();
        stream = stream.filter(s -> {
                    boolean ok = s.length() < 4;
                    System.out.println("Checking length element: " + s + " [" + ok + "]");
                    return ok;
                });

        stream.filter(s -> {
                    boolean ok = s.startsWith("a");
                    System.out.println("Checking startsWith element: " + s + " [" + ok + "]");
                    return ok;
                })
                .forEach(s -> {
                    System.out.println(s);
                })
        ;

        Predicate<String> strLenPredicate = s -> s.length() < 4;
        stringList.stream()
                .filter(strLenPredicate)
                .filter(s -> s.startsWith("a"))
                .forEach(s -> System.out.println(s));

        for (String s : stringList){
            if (s.length() > 3){
                continue;
            }
            if (!s.startsWith("a")){
                continue;
            }
            System.out.println(s);
        }

    }

    @Test
    public void streamMapFunction(){
        stringList.stream()
                .filter(s -> s.startsWith("a"))
                .map(s -> s.length())
                .filter(i -> i < 4)
                .forEach(i -> System.out.println(i));

        for(String s : stringList){
            if(!s.startsWith("a")){
                continue;
            }
            int l = s.length();
            if(l > 3){
                continue;
            }
            System.out.println(l);
        }
    }

    @Test
    public void sortingStream(){
        stringList.stream()
                .filter(s -> s.length() < 4)
                .map(s -> s.length())
                .sorted()
                .forEach(i -> System.out.println(i));
    }

    @Test
    public void streamMatchers(){
        boolean anyMatch = stringList.stream()
                .filter(s -> s.startsWith("a"))
                //.anyMatch(s -> s.startsWith("a"));
                .noneMatch(s -> s.startsWith("z"));
                //.allMatch(s -> s.startsWith("a"));

        System.out.println(anyMatch);

        boolean allMatch = stringList.stream().allMatch(s -> s.startsWith("a"));

        boolean tmp = true;
        for (String s : stringList){
            if(!s.startsWith("a")){
                tmp = false;
                break;
            }
        }

    }

    @Test
    public void countStreams(){
        long count = stringList.stream()
                .filter(s -> s.startsWith("x"))
                .count();
        System.out.println(count);
    }

    @Test
    public void reduceStream(){
        Optional<String> stringOptional = stringList.stream()
                .filter(s -> s.length() == 4)
                .sorted()
                .reduce((s1, s2) -> {
                    System.out.println("s1: " + s1);
                    System.out.println("s2: " + s2);
                    return s1 + " :: " + s2;
                });

        System.out.println(stringOptional);

        System.out.println(stringList.stream()
                .filter(s -> s.length() == 4)
                .map(s -> s.length())
                .reduce((i1, i2) -> {
                    System.out.println("i1: " + i1);
                    System.out.println("i2: " + i2);
                    return i1 + i2;
                })
        );
    }

    @Test
    public void parallelStream(){
        Optional<Integer> reduce = stringList.parallelStream()
                .map(s -> s.length())
                .reduce((i1, i2) -> {
                    String name = Thread.currentThread().getName();
                    System.out.println(name + " i1: " + i1);
                    System.out.println(name + " i2: " + i2);
                    return i1 + i2;
                });

        System.out.println(reduce);

        System.out.println(stringList.stream()
                .map(s -> s.length())
                .reduce((i1, i2) -> {
                    String name = Thread.currentThread().getName();
                    System.out.println(name + " i1: " + i1);
                    System.out.println(name + " i2: " + i2);
                    return i1 + i2;
                })
        );

    }


}

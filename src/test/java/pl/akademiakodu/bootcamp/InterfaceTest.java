package pl.akademiakodu.bootcamp;

import org.junit.Test;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class InterfaceTest {

    @Test
    public void anonymousObjects() {

        Formula formula = new Formula() {

            @Override
            public double calculate(int number) {
                return sqrt(number * number);
            }
        };

        System.out.println(formula.calculate(100));
        System.out.println(formula.sqrt(100));

    }

    @Test
    public void comparatorLambda() {
        List<String> list = Arrays.asList("zxc", "abc", "cde", "asd");

        System.out.println(list);

        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });

        System.out.println(list);
        Collections.sort(list, (String o1, String o2) -> {
            return o1.compareTo(o2);
        });

        Collections.sort(list, (String o1, String o2) -> o1.compareTo(o2));

        Collections.sort(list, (s1, s2) -> s1.compareTo(s2));

        System.out.println(list);
    }

    @Test
    public void myInterface() {
        Integer i1 = 10;
        Integer i2 = 20;

        MyComparator myComparator = new MyComparatorImpl();
        System.out.println(myComparator.compare(i1, i2));


        MyComparator<Integer, Boolean> myComparator2 = ((x1, x2) -> i1 < i2);
    }

    @Test
    public void compareDifferentTypes() {
        Integer integer = 20;
        String string = "20";

        BiComparator<Integer, String, Integer> biComparator =
                new BiComparatorImpl();
        System.out.println(biComparator.compare(integer, string));

        BiComparator<Integer, String, Integer> biComparator2 =
                (i, s) -> i - Integer.valueOf(s);
        System.out.println(biComparator2.compare(integer, string));

    }

    @Test
    public void staticMethodReference() {
        String stringIn = "123";
        //s -> Integer.valueOf(s)
        Converter<String, Integer> converter = Integer::valueOf;
        System.out.println(converter.convert(stringIn));
    }

    @Test
    public void objectMethodReference() {
        //Stworzyc klase StringUtils
        //Stworzyc metode getFirsLetterUppercase od Stringa

        String s = "asd"; // "asd" -> "A"
        StringUtils stringUtils = new StringUtils();

//        Optional<String> str = stringUtils.getFirsLetterUppercase(s);
//        if (str.isPresent()) {
//            str.get().length();
//            System.out.println(str.get());
//        }

        //s2 -> stringUtils.getFirsLetterUppercase(s2);
        Converter<String, Optional<String>> converter =
                stringUtils::getFirsLetterUppercase;

        System.out.println(converter.convert(s).orElse("empty string"));

    }

    @Test
    public void constructorReference() {
        Person p = new Person("a", "b");
        Person pp = new Person("a");

        //(firstName, lastName) -> new Person(firstName, lastName);
        PersonFactory<Person> personFactory = Person::new;

        Person p2 = personFactory.create("Jan", "Kowalski");

        System.out.println("p: " + p + ", p2: " + p2);
    }

    @Test
    public void lambdaVariables() {
        int i = 1;

        Converter<Integer, String> converter =
                (in) -> String.valueOf(in + i);

        //i = 2;

        System.out.println(converter.convert(2));

        //To sie nie skompiluje
        //Formula formula = x -> sqrt(x);
    }

    @Test
    public void predicateTest() {
        Predicate<String> lenPredicate = s -> s.length() > 5;
        System.out.println(lenPredicate.test("asdzxc"));

//        boolean empty = true;
//        boolean notEmpty = !empty;

        Predicate<String> nonNull = Objects::nonNull;
        Predicate<String> isEmpty = String::isEmpty;
        Predicate<String> notEmpty = s -> !s.isEmpty();
        Predicate<String> notEmpty2 = isEmpty.negate();
        Predicate<String> notEmptyAndLenGt5 = isEmpty.negate().and(lenPredicate);

        System.out.println(notEmptyAndLenGt5.test("asd546456"));

    }

    @Test
    public void functionTest() {
        Integer integer = 10;
        StringUtils stringUtils = new StringUtils();

        Function<Integer, String> toString =
                i -> {
                    System.out.println("Convert to String: " + i);
                    String s = String.valueOf(i);
                    System.out.println("After convert: " + s);
                    return s;
                };

        Function<String, String> stringStringFunction =
                s -> {
                    System.out.println("Get first letter: " + s);
                    String fl = stringUtils.getFirsLetterUppercaseNoOptional(s);
                    System.out.println("After convert: " + fl);
                    return fl;
                };
        Function<String, Integer> toInteger =
                s -> {
                    System.out.println("Convert to Integer: " + s);
                    Integer i = Integer.valueOf(s);
                    System.out.println("After convert: " + i);
                    return i;
                };

        System.out.println("Starting operation...");

        Function<Integer, Integer> convert = toString
                .andThen(stringStringFunction)
                .andThen(toInteger);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        System.out.println(convert.apply(10).getClass().getSimpleName());

    }

    @Test
    public void supplierAndConsumerTest(){
        Supplier<Person> personSupplier = Person::new;
        String imie = "imie";
        Supplier<Person> personSupplier2 =
                () -> new Person(imie, "nazwisko");

        //System.out.println(personSupplier2.get());

        Consumer<Person> printer = s -> System.out.println("Person: " + s);

        printer.accept(personSupplier2.get());
    }

    @Test
    public void comparatorTest(){
        Person p1 = new Person("John", "Smith");
        Person p2 = new Person("Jerry", "Smith");

        Comparator<Person> personComparator =
                (pp1, pp2) -> pp1.getLastName().compareTo(pp2.getLastName());

        System.out.println(personComparator.compare(p1, p2));
    }


}
